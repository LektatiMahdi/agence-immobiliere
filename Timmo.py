
""" --------------------- Ce projet est crée par LEKTATI Mahdi et Aubrey Timothée ---------------------- """

from os import system, name
from time import sleep 
import sys, getpass, shutil
from Classes.Personnes import Entreprise
from Classes.Agence import Agence

agence = Agence()

def clear(): 
    # for windows 
    if name == 'nt': 
        _ = system('cls') 
    # for mac and linux
    else: 
        _ = system('clear') 

def print_centre(s):
    print(s.center(shutil.get_terminal_size().columns))


"""
    -------------------------------------------------------------------------------------
                                    Buyer menu section
    -------------------------------------------------------------------------------------
"""
def client_buyer_menu():
    choices = """
                1) Indiquer vos préférences
                2) Consulter vos rendez-vous
                3) Voir les biens à vendre selon les préferences
                4) Voir les biens à vendre
                5) Acheter un bien
                6) Signer la promesse de vente
                7) Annuler l'achat
                0) Quitter
              """
    print(choices)

def client_buyer_space(user):
    print_centre("BIENVENUE")
    agence.setClientAcheteur(user)
    
    while True:
        client_buyer_menu()
        while True:
            try:
                choice = int(input("\tEntrer votre choix \n ? "))
                break
            except ValueError:
                pass
        clear()
        if choice==1:
            agence.client_actuel.preciserCriteres()
            if isinstance(agence.client_actuel, Entreprise):
                agence.Clients.update_user(agence.client_actuel, agence.client_id, True)
            else:
                agence.Clients.update_user(agence.client_actuel, agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
            
        elif choice==2:
            agence.client_actuel.show_Rdv(agence.client_id, True)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==3:
            agence.client_actuel.afficher_biens_selon_preferences(agence.informerAcheteursPotentiels(), agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
            
        elif choice==4:
            agence.show_liste_biens()
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==5:
            agence.client_actuel.realiserAchat(agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==6:
            agence.client_actuel.signer_promesse(agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==7:
            agence.client_actuel.seRetirerVente(agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==0:
            clear()
            print_centre("AU REVOIR.")
            sleep(0.2)
            sys.exit(0)
        
        else:
            print("\n CHOIX INVALIDE.\n Réessayer !!")
            input(" -- > Cliquez sur Entrée pour revenir au menu ")

        clear()
"""
    -------------------------------------------------------------------------------------
                                    Seller menu section
    -------------------------------------------------------------------------------------
"""
def client_seller_menu():
    choices = """
                1) Contacter l'agence (fixer un rendez-vous)
                2) Consulter vos rendez-vous
                3) Définir les détails du bien à vendre
                4) Signer mandat de vente
                5) Signer la promesse de vente
                6) Consulter vos Biens
                0) Quitter
              """
    print(choices)

def client_seller_space(user):
    print_centre("BIENVENUE")
    agence.setClientVendeur(user)
    
    while True:
        client_seller_menu()
        while True:
            try:
                choice = int(input("\tEntrer votre choix \n ? "))
                break
            except ValueError:
                pass
        clear()
        
        if choice==1:
            agence.client_actuel.prendreRDV(agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
            
        elif choice==2:
            agence.client_actuel.show_Rdv(agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
            
        elif choice==3:
            agence.client_actuel.enregistrerVente(agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==4:
            agence.client_actuel.signer_mandat(agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==5:
            agence.client_actuel.signer_promesse(agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==6:
            agence.client_actuel.consulter_biens(agence.client_id)
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==0:
            print_centre("AU REVOIR.")
            sleep(0.2)
            sys.exit(0)
        
        else:
            print("\n CHOIX INVALIDE.\n Réessayer !!")
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        clear()

"""
    -------------------------------------------------------------------------------------
                                    Admin menu section
    -------------------------------------------------------------------------------------
"""
def admin_menu():
    choices = """
                1)  Voir liste des clients
                2)  Voir seulement la liste des acheteurs
                3)  Voir seulement la liste des vendeurs
                4)  Gérer les clients (Ajouter/Modifier/Supprimer)
                5)  Consulter la liste des rendez-vous
                6)  Voir tous les biens immobiliers
                7)  Voir que les biens immobiliers vendues
                8)  Voir que les biens immobiliers en vente
                9)  Gérer les biens (Ajouter/Modifier/Supprimer)
                10) Voir liste des publicités publiées
                11) Gérer la publicité (Creer/Annuller)
                0)  Quitter
              """
    print(choices)

def admin_space():
    clear()
    print_centre("BIENVENUE ADMIN")
    
    while True:
        admin_menu()
        while True:
            try:
                choice = int(input("\tEntrer votre choix \n ? "))
                break
            except ValueError:
                pass
        clear()
        if choice==1:
            agence.show_clients()
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
            
        elif choice==2:
            agence.show_only_buyer_clients()
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
            
        elif choice==3:
            agence.show_only_seller_clients()
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==4:
            agence.gerer_client_menu()
        
        elif choice==5:
            agence.show_liste_rendez_vous()
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==6:
            agence.show_liste_biens()
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==7:
            agence.show_liste_biens_vendues()
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==8:
            agence.show_liste_biens_en_vente()
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
            
        elif choice==9:
            agence.gerer_biens_menu()
        
        elif choice==10:
            agence.show_liste_pub()
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        elif choice==11:
            agence.gerer_publicites_menu()
        
        elif choice==0:
            clear()
            print_centre("AU REVOIR.")
            sleep(0.2)
            sys.exit(0)
        
        else:
            print("\n CHOIX INVALIDE.\n Réessayer !!")
            input(" -- > Cliquez sur Entrée pour revenir au menu ")
        
        clear()

"""
    -------------------------------------------------------------------------------------
                                    Login control
    -------------------------------------------------------------------------------------
"""
def login():
    clear()
    new_client = False
    user, passw = None, None
    
    print_centre("Login !")
    print("")
    print("-- Si vous êtes nouveau client. appuyez sur Entrée !")
    print("")
    
    user = input("\t\t -- Nom d'utilisateur : ")
    if user == "":
        new_client = True
    else:
        passw = getpass.getpass("\t\t -- Mot de passe      : ")

    if user == "Admin" and passw == "Admin":
        clear()
        print_centre("Connexion réussie!")
        sleep(0.3)
        admin_space()
    elif new_client:
        clear()
        agence.register_client()
        login()
    else:
        user_exist = agence.check_user(user)
        if len(user_exist) != 0:
            if passw == user_exist[3]:
                if user_exist[1] == "acheteur" or (user_exist[1] == "entreprise" and user_exist[14].lower() == "acheteur"):
                    clear()
                    print_centre("Connexion réussie!")
                    sleep(0.3)
                    client_buyer_space(user_exist)
                elif user_exist[1] == "vendeur" or (user_exist[1] == "entreprise" and user_exist[14].lower() == "vendeur"):
                    clear()
                    print_centre("Connexion réussie!")
                    sleep(0.3)
                    client_seller_space(user_exist)
            else:
                print_centre("Connexion échouée (nom d'utilisateur ou mot de passe incorrect!)")
                login()
        else:
            sleep(1)
            login()

"""
    -------------------------------------------------------------------------------------
                                    MAIN PROGRAM
    -------------------------------------------------------------------------------------
"""
if __name__ == '__main__':
    login()
