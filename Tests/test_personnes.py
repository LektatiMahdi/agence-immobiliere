import os, sqlite3, pytest
from time import sleep
from Database.gestions_personnes import *


class TestClients:
    conn = sqlite3.connect("test_sqlite.db")
    cursor = conn.cursor()

    # Initialisation :
    new_vendeur = Vendeur("nom", "password", 7888888888, "adresse", "mail")

    # CREATE a table
    cursor.execute("""
            CREATE TABLE IF NOT EXISTS Personnes (
                personne_id INTEGER PRIMARY KEY AUTOINCREMENT,
                type TEXT,
                nom TEXT,
                password TEXT,
                numTel INTEGER,
                adresse TEXT,
                mail TEXT,
                prixSouhaite REAL,
                localisation TEXT,
                surfaceSol INTEGER,
                nbPieces INTEGER,
                typeBienVoulu TEXT,
                numSIREN INTEGER,
                formeJuridique TEXT,
                statut TEXT
            )
          """)

    """ Tests that we can successfully create an item(client) """

    def test_create_client(self):
        password = password_encrypt(self.new_vendeur.password.encode(), 'mypass')
        with self.conn:
            self.cursor.execute("""
                      INSERT INTO Personnes
                            ( type,
                            nom,
                            password,
                            numTel,
                            adresse,
                            mail
                            )
                          VALUES (:type, :nom, :password, :numTel, :adresse, :mail)
                          """,
                            {
                                'type': 'vendeur',
                                'nom': self.new_vendeur.nom,
                                'password': password,
                                'numTel': self.new_vendeur.numTel,
                                'adresse': self.new_vendeur.adresse,
                                'mail': self.new_vendeur.mail
                            })

        self.cursor.execute("SELECT * FROM Personnes")
        actual = self.cursor.fetchall()
        mdp_decrypte = password_decrypt(actual[0][3], 'mypass').decode()  # decrypt the token
        liste = []
        my_tuple = (actual[0][0],)
        for i in range(1,15):
            if i == 3:
                my_tuple = my_tuple + (mdp_decrypte,)
            else :
                my_tuple = my_tuple + (actual[0][i],)
        liste.append(my_tuple)

        expected = [(1, 'vendeur', "nom", "password", 7888888888, "adresse", "mail", None, None, None, None, None, None, None, None)]

        assert expected == liste


    """ Tests that we can successfully remove an item(client) """

    def test_remove_client(self):
        expected = []

        with self.conn:
            self.cursor.execute("DELETE from Personnes WHERE personne_id=?", (1,))

        self.cursor.execute("SELECT * FROM Personnes")
        actual = self.cursor.fetchall()

        self.conn.close()

        assert expected == actual

""" Delete the database """
def teardown_module(module):
    os.remove('test_sqlite.db')



