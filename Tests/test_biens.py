import os, sqlite3, pytest
from time import sleep
from Database.gestions_biens import *

class TestBiens:
    
    conn = sqlite3.connect("test_sqlite.db")
    cursor = conn.cursor()
        
    # Initialisation :
    new_appt = Appt("toulouse", 500, "sud", "20-10-21", "20-10-22", 8, 9, 6000)
    maison = Maison("toulouse-modifieé", 1000, "sud", "20-10-21", "20-10-22", 400, 8, 9, "air")
        
    # CREATE a table
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS Biens (
                bien_id INTEGER PRIMARY KEY AUTOINCREMENT,
                type TEXT,
                adresse TEXT,
                orientation TEXT,
                dateVente TEXT,
                dateDispo TEXT,
                status TEXT,
                prix REAL,
                surfaceSol REAL,
                longeurFacade REAL,
                surfaceHabitable REAL,
                nbPieces INTEGER,
                nbEtages INTEGER,
                moyenChauffage TEXT,
                chargesMensuelles REAL,
                "id_vendeur" INTEGER REFERENCES Personnes(personne_id)
            )
    """)
        
    
    """ Tests that we can successfully create an item(bien) """
    def test_create_bien(self):
        
        with self.conn:
            self.cursor.execute("""
                      INSERT INTO Biens
                        ( type,
                        adresse,
                        orientation,
                        dateVente,
                        dateDispo,
                        status,
                        prix,
                        nbPieces,
                        nbEtages,
                        chargesMensuelles,
                        id_vendeur
                        )
                      VALUES (:type, :adresse, :orientation, :dateVente, :dateDispo, :status, :prix, :nbPieces, :nbEtages, :chargesMensuelles, :id_vendeur)
                      """,
                        {
                            'type': 'appart',
                            'adresse': self.new_appt.adresse,
                            'orientation': self.new_appt.orientation, 
                            'dateVente': self.new_appt.dateVente, 
                            'dateDispo': self.new_appt.dateDispo, 
                            'status': self.new_appt.status, 
                            'prix': self.new_appt.prix,
                            'nbPieces': self.new_appt.nbPieces,
                            'nbEtages': self.new_appt.nbEtages,
                            'chargesMensuelles': self.new_appt.chargesMensuelles,
                            'id_vendeur': 1
                        })
        
        self.cursor.execute("SELECT * FROM Biens")
        actual =  self.cursor.fetchall()
        expected = [(1, "appart", "toulouse", "sud", "20-10-21", "20-10-22", "En vente", 500, None, None, None, 8, 9, None, 6000, 1)]
        
        assert expected == actual
    
    """ Tests that we can successfully update an item(bien) """
    def test_update_bien(self):
        sqlite_update_query = """
                      UPDATE Biens SET 
                        type=:type,
                        adresse=:adresse, 
                        orientation=:orientation,
                        dateVente=:dateVente, 
                        dateDispo=:dateDispo, 
                        status=:status, 
                        prix=:prix, 
                        surfaceHabitable=:surfaceHabitable,
                        nbPieces=:nbPieces,
                        nbEtages=:nbEtages,
                        moyenChauffage=:moyenChauffage
                      WHERE
                        bien_id=:bien_id
                    """
        
        new_data = {
            'bien_id': 1,
            'type': 'maison',
            'adresse': self.maison.adresse,
            'orientation': self.maison.orientation, 
            'dateVente': self.maison.dateVente, 
            'dateDispo': self.maison.dateDispo, 
            'status': self.maison.status, 
            'prix': self.maison.prix,
            'surfaceHabitable': self.maison.surfaceHabitable,
            'nbPieces': self.maison.nbPieces,
            'nbEtages': self.maison.nbEtages,
            'moyenChauffage': self.maison.moyenChauffage,
        }                   
        with self.conn:
            self.cursor.execute(sqlite_update_query, new_data)
        
        expected = [(1, "maison", "toulouse-modifieé", "sud", "20-10-21", "20-10-22", "En vente", 1000.0, None, None, 400, 8, 9, "air", 6000, 1)]
        
        self.cursor.execute("SELECT * FROM Biens")
        actual = self.cursor.fetchall()
        
        assert expected == actual
    
    """ Tests that we can successfully get al items(bien) """
    def test_get_bien(self):
        expected = [(1, "maison", "toulouse-modifieé", "sud", "20-10-21", "20-10-22", "En vente", 1000.0, None, None, 400, 8, 9, "air", 6000, 1)]
        
        self.cursor.execute("SELECT * FROM Biens")
        actual = self.cursor.fetchall()
        
        assert expected == actual
    
    """ Tests that we can successfully delete an item(bien) """
    def test_remove_bien(self):
        expected = []
    
        with self.conn:
            self.cursor.execute("DELETE from Biens WHERE bien_id=?", (1,))
        
        self.cursor.execute("SELECT * FROM Biens")
        actual = self.cursor.fetchall()
        
        self.conn.close()
        
        assert expected == actual
    
""" Delete the database """
def teardown_module(module):
    print('******TEARDOWN******')
    os.remove("test_sqlite.db")