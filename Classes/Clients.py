from Database.gestions_personnes import *

class Gestion_clients():
    def add_user(self, personne, is_entreprise=False):
        insert_personne(personne, is_entreprise)
    
    def get_user_by_type(self, type):
        return get_personne_by_type(type)
    
    # get all table data
    def get_users(self):
        return get_personnes()
    
    # return user with id 'user_id'
    def get_user(self, user_id):
        return get_personne(user_id)
    
    def update_user(self, personne, id_personne, is_entreprise=False):
        update_personne(personne, id_personne, is_entreprise)
    
    def remove_user(self, id_personne):
        remove_personne(id_personne)
    
    def login_user(self, username):
        return get_user(username)


def print_client(client):
    print(f"id                  : {client[0]}")
    print(f"Client              : {client[1]}")
    
    if client[1] == 'entreprise':
        print(f"Statut              : {client[14]}")
        print(f"Numéro SIREN        : {client[12]}")
        print(f"Forme juridique     : {client[13]}")
    
    print(f"Nom                 : {client[2]}")
    print(f"Numéro de téléphone : {client[4]}")
    print(f"Adresse             : {client[5]}")
    print(f"Mail                : {client[6]}")
    
    print("-" * 80)