import datetime, shutil
import getpass
from sqlite3.dbapi2 import Timestamp
from sys import exit
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from Classes.Clients import Gestion_clients
from Classes.Ventes import *
from Classes.Biens import *
from Classes.Rdv import *

""" ------------------------ Class to present a person client (parent) ----------------------------- """
class Personne(object):
    """ Util variables initialisation """
    gestion_rdv     = Gerer_rdv()
    gestion_contrat = Gestion_contrat()
    vente           = Gestion_ventes()
    
    def __init__(self, nom, password, numTel, adresse, mail):
        self.nom = nom
        self.password = password
        self.numTel = numTel
        self.adresse = adresse
        self.mail = mail

    """ allows user to set a meeting with agency """
    def erreurPermission(self):
        print(f"Erreur ! Essayer d'autres choix !")
    

    """ allows user to see all his meetings """
    def show_Rdv(self, client_id, is_buyer=False):
        if is_buyer:
            list_rdv = self.gestion_rdv.get_by_client_acheteur(client_id)
        else:
            list_rdv = self.gestion_rdv.get_by_client_vendeur(client_id)
        space = "-" * 50
        print_centre(space)
        print_centre("Vos rendez-vous")
        print_centre(space)
        
        for elem in list_rdv:
            print("id       : ", elem[0])
            print("Horaire  : ", elem[1])
            print("-" * 50)

""" -------------------------------------- Buyer class  ---------------------------------------- """
class Acheteur(Personne):
    
    """ available types for property """
    types_disponibles = ("maison", "appartement", "terrain")
    
    def __init__(self, nom, password, numTel, adresse, mail, details=None):
        Personne.__init__(self, nom, password, numTel, adresse, mail)
        if details:
            self.typeBienVoulu = details[11]
            self.prixSouhaite = details[7]
            self.localisation = details[8]
            self.surfaceSol = details[9]
            self.nbPieces = details[10]
        else:
            self.typeBienVoulu = None
            self.prixSouhaite = None
            self.localisation = None
            self.surfaceSol = None
            self.nbPieces = None
    
    """ allows buyer to describe his prefrences """
    def preciserCriteres(self):
        print_centre("-------------------------- Je précise mes critères --------------------------")

        self.typeBienVoulu = input("-- Quel type de bien souhaitez-vous acquérir ? (Maison / Appartement / Terrain) : ")
        while (self.typeBienVoulu.lower() not in self.types_disponibles):
            self.typeBienVoulu = input("Ce type de bien n'existe pas ...\nQuel type de bien souhaitez-vous acquérir(Maison / Appartement / Terrain)\n ? ")
            
        while True:
            try:
                self.prixSouhaite = float(input("-- Quel est le prix du bien que vous souhaitez acquérir ? "))
                while (self.prixSouhaite < 0):
                    self.prixSouhaite = float(input("Le prix du bien ne peut pas être négatif ... Quel est le prix du bien que souhaitez-vous acquérir ?"))
                break
            except ValueError:
                print(" -- > Entrer un nombre !")
        
        self.localisation = input("-- Quelle est la localisation du bien que vous souhaitez acquérir ? ")

        if self.typeBienVoulu.lower() == "maison":
            while True:
                try:
                    self.surfaceSol = int(input("-- Quelle surface au sol désirez-vous (en m2) ? "))
                    while (self.surfaceSol < 0):
                        self.surfaceSol = int(input("La surface au sol du bien ne peut pas être négative ...Quelle surface au sol désirez-vous (en m2) ? "))
                    break
                except ValueError:
                    print(" -- > Entrer un nombre !")
            
            while True:
                try:
                    self.nbPieces = int(input("-- Combien de pièces souhaitez-vous ?"))
                    while (self.nbPieces < 0):
                        self.nbPieces = int(input("Le nombre de pièce ne peut pas être négatif ... Combien de pièces souhaitez-vous ? "))
                    break
                except ValueError:
                    print(" -- > Entrer un nombre !")

        elif self.typeBienVoulu.lower() == "terrain":
            while True:
                try:
                    self.surfaceSol = int(input("-- Quelle surface au sol désirez-vous (en m2) ? "))
                    while (self.surfaceSol < 0):
                        self.surfaceSol = int(input("Le surface au sol du bien ne peut pas être négative ... Quelle surface au sol désirez-vous (en m2) ? "))
                    break
                except ValueError:
                    print(" -- > Entrer un nombre !")

        else:
            while True:
                try:
                    self.nbPieces = int(input("-- Combien de pièces souhaitez-vous ? "))
                    while (self.nbPieces < 0):
                        self.nbPieces = int(input("Le nombre de pièce ne peut pas être négatif ... Combien de pièces souhaitez-vous ? "))
                    break
                except ValueError:
                    print(" -- > Entrer un nombre !")

    """ allows user to set a meeting with buyer """
    def prendreRDV(self, client_id1, client_id2):
        date = input("-- Entrer date voulu du rdv (aaaa-mm-jj): ")

        print_centre("-- Horaires Disponible")
        available_hours = self.gestion_rdv.get_available_time(date)
        for hour in available_hours:
            print(hour)

        hour = input("-- Entrer heure voulu (H:M:S): ")
        final_date = date + " " + hour
        Rendez_vous = Rdv(final_date, client_id1, client_id2)
        self.gestion_rdv.save(Rendez_vous)

    """ check if date is equal to actual hour """
    def check_available_right_to_sign_contract(self, acheteur_id):
        now = datetime.datetime.now()
        rdv = self.gestion_rdv.get_by_client_acheteur(acheteur_id)
        timestampStr = now.strftime("%Y-%m-%d")

        for elem in rdv:
            rdv_hour = int(elem[1].split()[1].split(":")[0])
            # Check if current day and current hour are the same with meeting data
            if rdv_hour == now.hour and timestampStr == elem[1].split()[0]:
                return True

        return False

    """ allows buyer to make a purchase """
    def realiserAchat(self, id_acheteur):
        id_bien = int(input("-- Entrer l'id du bien que vous voulez acheter : "))
        bien = self.vente.get_vendeur_by_id(id_bien)
        if len(bien) > 0:
            id_vendeur = bien[0][0]
            self.prendreRDV(id_vendeur, id_acheteur)
        
            promesse_initial = initiate_promesse(id_vendeur, id_acheteur, id_bien, self.verser(id_bien))
            self.gestion_contrat.create_promesse(promesse_initial)
        else:
            print(f" -- > Aucune bien n'est enregistré avec l'id {id_bien} !")

    """ allows buyer to cancel purchase """
    def seRetirerVente(self, id_acheteur):
        promesse = self.gestion_contrat.get_promesse_by_acheteur(id_acheteur)
        if len(promesse) > 0:
            self.gestion_contrat.remove_promesse(id_acheteur)
            print(" -- > La promesse de vente a été annullé !")
        else:
            print(" -- > Aucune promesse de vente n'est enregistré !")
    
    """ allows buyer to pay purchase price """
    def verser(self, id_bien):
        bien = self.vente.get_item(id_bien)[0]
        prix_du_bien = bien[7]
        prix_verse = prix_du_bien * 0.1     # L'acheteur doit verser 10% du prix de vente du bien
        
        return prix_verse
    
    """ allows buyer to sign agreement to sell """
    def signer_promesse(self, id_acheteur):
        if self.check_available_right_to_sign_contract(id_acheteur):
            promesse = self.gestion_contrat.get_promesse_by_acheteur(id_acheteur)
            if len(promesse) > 0:
                id_promesse = promesse[0][0]
                id_bien     = promesse[0][7]
                id_vendeur  = promesse[0][9]
                prix_verse  = promesse[0][2]
                est_pctge_versee = promesse[0][6]
                updated_promess = scan_promesse(id_vendeur, id_acheteur, id_bien, prix_verse)
                updated_promess.est_pourcentage_versee = est_pctge_versee
                self.gestion_contrat.update_promesse_data(id_promesse, updated_promess)
                
                print(" -- > La promesse de vente est bien signée !")
            else:
                print(" -- > Aucune promesse de vente n'est enregistré ! Prenner un rendez-vous")
        else:
            print_centre("Attender l'heure de votre rendez-vous pour signer la promesse de vente !!")


    """ allows buyer to display properties according to preferences """
    def afficher_biens_selon_preferences(self, liste_biens, id_acheteur):
        space = "-" * 60
        print_centre(space)
        print_centre("Liste de bien à vendre qui peuvent t'intéresser")
        print_centre(space)
        for bien in liste_biens[id_acheteur]:
            print_bien(bien)


""" -------------------------------------- Seller class  ---------------------------------------- """
class Vendeur(Personne):
    
    def __init__(self, nom, password, numTel, adresse, mail):
        Personne.__init__(self, nom, password, numTel, adresse, mail)
    
    """ allows user to set a meeting with agency """
    def prendreRDV(self, client_id):
        date = input("-- Entrer date voulu du rdv (aaaa-mm-jj): ")
        
        print_centre("-- Horaires Disponible --")
        available_hours = self.gestion_rdv.get_available_time(date)
        for hour in available_hours:
            print(hour)
        
        hour = input("-- Entrer heure voulu (H:M:S): ")
        final_date = date + " " + hour
        Rendez_vous = Rdv(final_date, client_id)
        self.gestion_rdv.save(Rendez_vous)
    
    """ check if date is equal to actual hour """
    def check_available_right_to_scan_data(self, vendeur_id):
        now = datetime.datetime.now()
        rdv = self.gestion_rdv.get_by_client_vendeur(vendeur_id)
        timestampStr = now.strftime("%Y-%m-%d") 
        
        for elem in rdv:
            rdv_hour = int(elem[1].split()[1].split(":")[0])
            # Check if current day and current hour are the same with meeting data
            if rdv_hour == now.hour and timestampStr == elem[1].split()[0]:
                return True
                
        return False

    """ allows seller to add a property for sale """
    def enregistrerVente(self, vendeur_id):
        if self.check_available_right_to_scan_data(vendeur_id):
            bien = scan_bien()
            self.vente.add_item(bien, vendeur_id)
        else:
            print_centre("Attender l'heure de votre rendez-vous pour définir les propriétés de votre bien !!")
    
    """ allows seller to sign sales mandat so that agency could act upon it """
    def signer_mandat(self, vendeur_id):
        if self.check_available_right_to_scan_data(vendeur_id):
            mandat = scan_mandat(vendeur_id)
            self.gestion_contrat.create_mandat(mandat)
        else:
            print_centre("Attender l'heure de votre rendez-vous pour signer le mandat de vente !!")

    def consulter_biens(self, vendeur_id):
        list_bien = self.vente.get_items_by_vendeur(vendeur_id)
        print("-" * 80)
        print_centre("Liste de vos biens")
        print("-" * 80)
        for bien in list_bien:
            print_bien(bien)
    
    """ allows seller to sign agreement to sell """
    def signer_promesse(self, vendeur_id):
        if self.check_available_right_to_scan_data(vendeur_id):
            promesse = self.gestion_contrat.get_promesse_by_vendeur(vendeur_id)
            if len(promesse) > 0:
                id_promesse     = promesse[0][0]
                adresseNotaire  = promesse[0][1]
                prixVerse       = promesse[0][2]
                dateDeVente     = promesse[0][3]
                comissionAgence = promesse[0][4]
                fraisDeVente    = promesse[0][5]
                bien_id         = promesse[0][7]
                acheteur_id     = promesse[0][8]
                
                updated_promess = initiate_promesse(vendeur_id, acheteur_id, bien_id, prixVerse)
                updated_promess.est_pourcentage_versee  = 1
                updated_promess.adresseNotaire          = adresseNotaire
                updated_promess.dateDeVente             = dateDeVente
                updated_promess.comissionAgence         = comissionAgence
                updated_promess.fraisDeVente            = fraisDeVente 
                
                self.gestion_contrat.update_promesse_data(id_promesse, updated_promess)
                
                self.vente.set_item_vendu(vendeur_id)
                print(" -- > La promesse de vente est bien signée !")
            else:
                print(" -- > Aucune promesse de vente n'est enregistré !")
        else:
            print_centre("Attender l'heure de votre rendez-vous pour signer le promesse de vente !!")


""" -------------------------------------- Company class  ---------------------------------------- """
class Entreprise(Acheteur, Vendeur):
    """ available types for company """
    statuts_possible = ("acheteur", "vendeur")
    
    def __init__(self, nom, password, numTel, adresse, mail, numSiren, formeJuridique, statut):
        if not statut.lower() in self.statuts_possible:
            print("statut non valide")
            exit(1)
        else:
            self.statut = statut
            self.numSIREN = numSiren
            self.formeJuridique = formeJuridique
            
            if statut.lower == "acheteur":
                Acheteur.__init__(self, nom, password, numTel, adresse, mail)
            else :
                Vendeur.__init__(self, nom, password, numTel, adresse, mail)
                self.typeBienVoulu  = None
                self.prixSouhaite   = None
                self.localisation   = None
                self.surfaceSol     = None
                self.nbPieces       = None

""" Function to center text in console """
def print_centre(s):
    print(s.center(shutil.get_terminal_size().columns))


def scan_client():
    type          = input("-- Le client est-il Acheteur ou un Vendeur ? ")  
    while not type.lower() in ("acheteur", "vendeur"):
        type = input("-- Le client est Acheteur ou un Vendeur (Entrer Acheteur/Vendeur) ? ")
    
    name          = input("\t\t -- Nom              : ")
    password      = getpass.getpass("\t\t -- Mot de passe     : ")
    mail          = input("\t\t -- mail             : ")
    adress        = input("\t\t -- adresse          : ")
    numTelephone  = input("\t\t -- Num de telephone : ")
    is_Entreprise = input("-- Le client est une entreprise (O/N) ? ")
    if is_Entreprise.lower() == 'o':
        numSiren       = input("\t\t -- Num SIREN        : ")
        formeJuridique = input("\t\t -- Forme Juridique  : ")
        status         = type
        
    if is_Entreprise.lower() == "o":
        return Entreprise(name, password, numTelephone, adress, mail, numSiren, formeJuridique,status)
    elif type.lower() == "acheteur":
        return Acheteur(name, password, numTelephone, adress, mail)
    else:
        return Vendeur(name, password, numTelephone, adress, mail)