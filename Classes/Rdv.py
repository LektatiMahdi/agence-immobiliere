from Database.gestion_rdv import *
import datetime as dt

class Rdv():
    def __init__(self, date, id1=None, id2=None):
        self.date = date # '%Y-%m-%d %H:%M:%S'
        self.id1  = id1
        self.id2  = id2

class Gerer_rdv():
    def save(self, rdv):
        insert_rdv(rdv)
    
    def update(self, id, new_rdv):
        update_rdv(id, new_rdv)
    
    def remove(self, id):
        remove_rdv(id)
    
    def get_all(self):
        return get_rdv_all()
    
    def get(self, id):
        return get_rdv(id)

    def get_by_client_vendeur(self, id_client):
        return get_rdv_by_client_vendeur(id_client)

    def get_by_client_acheteur(self, id_client):
        return get_rdv_by_client_acheteur(id_client)
    
    def get_available_time(self, date):
        start = "08"    # le reception de clients commence à 8h du matin
        end = "18"      # L'agence ferme à 18h
        work_hours = [] # Liste des horaires de travail
        while int(start) <= int(end):
            if start == '9':
                work_hours.append('09')
            else:
                work_hours.append(start)
            start = str(int(start) + 1)
        
        list_rdv = get_rdv_all()
        booked   = []
        for rdv in list_rdv:
            if rdv[1].split()[0] == date:
                booked.append(rdv[1])
        
        # booked.sort(key=lambda tup: tup[1])
        booked_hours = []
        # current_day  = booked[0][1].split()[0]
        for elem in booked:
            booked_hours.append(elem.split()[1].split(':')[0])
        
        available = [h for h in work_hours if h not in booked_hours]
        
        for i, v in enumerate(available):
            available[i] = date + " " + v + ":00:00"
        
        return available