from Database.gestion_pub import *

class Publicite():
    def add_item(self, pub, id_bien):
        insert_pub(pub, id_bien)
    
    # get all table data
    def get_items(self):
        return get_publicites()
    
    # get a row by id
    def get_item(self, pubId):
        return get_publicite(pubId)
    
    def update_item(self, pub, id_pub, id_bien):
        update_pub(pub, id_pub, id_bien)
    
    def remove_item(self, id):
        remove_pub(id)

class Media():
    def __init__(self, media, description=None, photo="", video=""):
        self.media       = media
        self.photo       = photo
        self.video       = video
        self.description = description

def scan_publicite():
    media_possible = ("Site internet", "Presse", "Journaux")
    
    media = input("Entrer type du media (Site internet, Presse, Journaux): ")
    while not media in media_possible:
        media = input("Entrer type du media (Site internet, Presse, Journaux): ")
    
    description = input("Entrer description : ")
    image       = input("Entrer chemin absolue du fichier : ")
    video       = input("Entrer chemin absolue du fichier : ")

    return Media(media, description, image, video)