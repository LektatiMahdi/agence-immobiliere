from Database.gestions_biens import *
from Database.gestion_contrat import *
from dataclasses import dataclass

""" ------------------------------- GESTON DES BIENS POUR LA VENTE ----------------------------- """
class Gestion_ventes():
    def add_item(self, bien, vendeur_id):
        insert_bien(bien, vendeur_id)
    
    def get_item(self, id):
        return get_bien(id)
    
    def get_items_by_type(self, type):
        return get_biens_by_type(type)

    def get_vendeur_by_id(self, id):
        return get_idVendeur_by_idBien(id)
    
    def get_items_by_vendeur(self, vendeur_id):
        return get_bien_by_vendeur(vendeur_id)
    
    def get_items_en_vente(self):
        return get_biens_en_vente()
    
    def get_items_vendues(self):
        return get_biens_vendues()
    
    # get all table data
    def get_items(self):
        return get_biens()
    
    def update_item(self, bien, id_bien, id_vendeur):
        update_bien(bien, id_bien, id_vendeur)
    
    def set_item_vendu(self, id_vendeur):
        update_bien_to_sold(id_vendeur)
    
    def remove_item(self, id_bien):
        remove_bien(id_bien)

""" ------------------------------- CONTRATS DE VENTES----------------------------- """
@dataclass
class MandatDeVente():
    duree: int
    id_vendeur: int

@dataclass
class PromesseDeVente():
    bien_id: int
    acheteur_id: int
    vendeur_id: int
    prixVerse: float
    adresseNotaire: str = None
    dateDeVente: str = None
    comissionAgence: float = None
    fraisDeVente : float = None
    est_pourcentage_versee: int = 0


class Gestion_contrat():
    def create_mandat(self, mandat):
        insert_mandat(mandat)
    
    def create_promesse(self, promesse):
        insert_promesse(promesse)
    
    def update_mandat_data(self, id, new_mandat):
        update_mandat(id, new_mandat)
    
    def update_promesse_data(self, id_promesse, new_promesse):
        update_promesse(id_promesse, new_promesse)
    
    def get_mandat_list(self):
        return get_all_mandat()
    
    def get_mandat(self, id):
        return get_mandat()
    
    def get_promesse_list(self):
        return get_all_promesses()
    
    def get_promesse(self, id):
        return get_promesse()
    
    def get_promesse_by_acheteur(self, id_acheteur):
        return select_promesse_by_acheteur(id_acheteur)
    
    def get_promesse_by_vendeur(self, id_vendeur):
        return select_promesse_by_vendeur(id_vendeur)
    
    def remove_mandat(self, id):
        remove_mandat(id)
    
    def remove_promesse(self, id_acheteur):
        remove_promesse(id_acheteur)

def scan_mandat(id_vendeur):
    duree = int(input("-- Entrer durée de mandat de vente (en mois): "))
    while duree < 0:
        duree = int(input("-- Entrer une valeur positive pour durée du mandat de vente (en mois): "))
    
    return MandatDeVente(duree, id_vendeur)

#TODO
""" return new promesse """
def scan_promesse(id_vendeur, id_acheteur, id_bien, prix_verse):
    adresseNotaire  = input("-- Entrer l'adresse du notaire               : ")
    dateDeVente     = input("-- Entrer la date de vente (dd-mm-yyyy)      : ")
    fraisDeVente    = float(input("-- Entrer les frais de vente                 : "))
    while fraisDeVente < 0:
        fraisDeVente    = float(input("-- Entrer les frais de vente (sup à 0)       : "))

    ComissionAgence = float(input("-- Entrer la comission de l'agence           : "))
    while ComissionAgence < 0:
        ComissionAgence = float(input("-- Entrer la comission de l'agence (sup à 0) : "))
    
    return PromesseDeVente(id_bien, id_acheteur, id_vendeur, prix_verse, 
                           adresseNotaire,
                           dateDeVente,
                           fraisDeVente,
                           ComissionAgence)

def initiate_promesse(id_vendeur, id_acheteur, id_bien, prix_verse):
    return PromesseDeVente(id_bien, id_acheteur, id_vendeur, prix_verse)