class Bien():
    status_possible = ("Vendu", "En vente")
    def __init__(self, adresse, prix, orientation, dateVente, dateDispo, status="En vente"):
        self.adresse     = adresse
        self.prix        = prix
        self.orientation = orientation
        self.dateVente   = dateVente
        self.dateDispo   = dateDispo
        self.status      = status
        # if not status in self.status_possible:
        #     print("status non valide")
        # else:
        #     self.status      = status


class Terrain(Bien):
    def __init__(self, adresse, prix, orientation, dateVente, dateDispo, surfaceSol, longeurFacade):
        Bien.__init__(self, adresse, prix, orientation, dateVente, dateDispo)
        self.surfaceSol    = surfaceSol
        self.longeurFacade = longeurFacade


class Maison(Bien):
    def __init__(self, adresse, prix, orientation, dateVente, dateDispo, surfaceHabitable, nbPieces, nbEtages, moyenChauffage):
        Bien.__init__(self, adresse, prix, orientation, dateVente, dateDispo)
        self.surfaceHabitable  = surfaceHabitable
        self.nbPieces          = nbPieces
        self.nbEtages          = nbEtages
        self.moyenChauffage    = moyenChauffage


class Appt(Bien):
    def __init__(self, adresse, prix, orientation, dateVente, dateDispo, nbPieces, nbEtages, chargesMensuelles):
        Bien.__init__(self, adresse, prix, orientation, dateVente, dateDispo)
        self.nbPieces          = nbPieces
        self.nbEtages          = nbEtages
        self.chargesMensuelles = chargesMensuelles
        
    def __repr__(self):
        return "Appt('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(
            self.adresse,
            self.prix,
            self.orientation,
            self.dateVente, 
            self.dateDispo,
            self.status,
            self.nbPieces,
            self.nbEtages,
            self.chargesMensuelles
            )

def scan_bien():
    """ initialisation du bien ainsi que les types dispo """
    bien = None
    types_disponibles = ("maison", "appartement", "terrain")
    
    type_bien = input("-- Type du bien : ")
    while type_bien.lower() not in types_disponibles:
        type_bien = input("-- Entrer un type valide du bien (Maison - Appartement - Terrain) : ")
    
    adresse     = input("-- Adresse du bien     : ")
    prix        = float(input("-- Prix voulu          : "))
    while prix < 0:
        prix = float(input("-- Prix voulu ( >0 ) : "))
    orientation = input("-- Orientation du bien : ")
    dateVente   = input("-- Date du mise en vente voulu (jj/mm/aaaa)    : ")
    dateDispo   = input("-- Date du disponibilité du bien (jj/mm/aaaa)  : ")
    
    if type_bien.lower() == "maison":
        surfaceHabitable  = int(input("-- Surface habitale du bien (m^2): "))
        while surfaceHabitable < 0 :
            surfaceHabitable  = int(input("-- Surface habitale superieur à 0 du bien (m^2) : "))
        
        nbPieces          = int(input("-- Nombre piéces du bien : "))
        while nbPieces < 0 :
            nbPieces      = int(input("-- Nombre piéces superieur à 0 du bien : "))
        
        nbEtages          = int(input("-- Nombre étages du bien : "))
        while nbEtages < 0 :
            nbEtages      = int(input("-- Nombre étages superieur à 0 du bien : "))
            
        moyenChauffage    = input("-- Moyen de chauffage du bien : ")
        
        bien = Maison(adresse, prix, orientation, dateVente, dateDispo, surfaceHabitable, nbPieces, nbEtages, moyenChauffage)
    
    elif type_bien.lower() == "appartement":
        nbPieces          = int(input("-- Nombre piéces du bien : "))
        while nbPieces < 0 :
            nbPieces      = int(input("-- Nombre piéces superieur à 0 du bien : "))
        
        nbEtages          = int(input("-- Nombre étages du bien : "))
        while nbEtages < 0 :
            nbEtages      = int(input("-- Nombre étages superieur à 0 du bien : "))
        
        chargesMensuelles = float(input("-- Charges mensuelles du bien : "))
        while chargesMensuelles < 0 :
            chargesMensuelles = float(input("-- Charges mensuelles superieur à 0 du bien : "))
        
        bien = Appt(adresse, prix, orientation, dateVente, dateDispo, nbPieces, nbEtages, chargesMensuelles)
    
    elif type_bien.lower() == "terrain":
        surfaceSol  = int(input("-- Surface sol du bien (m^2): "))
        while surfaceSol < 0 :
            surfaceSol  = int(input("-- Surface sol superieur à 0 du bien (m^2) : "))
        
        longeurFacade  = int(input("-- Longeur facade du bien (m^2): "))
        while longeurFacade < 0 :
            longeurFacade  = int(input("-- Longeur facade superieur à 0 du bien (m^2) : "))
        
        bien = Terrain(adresse, prix, orientation, dateVente, dateDispo, surfaceSol, longeurFacade)
    
    return bien

""" Print bien data """
def print_bien(bien):
    print(f'id                      : {bien[0]}')
    print(f'Type                    : {bien[1]}')
    print(f'Status                  : {bien[6]}')
    print(f'Prix                    : {bien[7]}')
    print(f'Adresse                 : {bien[2]}')
    print(f'Orientation             : {bien[3]}')
    print(f'Date de vente           : {bien[4]}')
    print(f'Date de disponibilité   : {bien[5]}')

    if bien[1] == 'terrain':
        print(f'Surface au sol          : {bien[8]}')
        print(f'Longeur facade          : {bien[9]}')
    elif bien[1] == 'maison':
        print(f'Surface Habitable       : {bien[10]}')
        print(f'Nombre de pièces        : {bien[11]}')
        print(f"Nombre d'étages         : {bien[12]}")
        print(f'Moyen de chauffage      : {bien[13]}')
    elif bien[1] == 'appart':
        print(f'Nombre de pièces        : {bien[11]}')
        print(f"Nombre d'étages         : {bien[12]}")
        print(f'Charges mensuelles      : {bien[14]}')
    
    print("-" * 80)