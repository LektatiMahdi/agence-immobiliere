from .Ventes import Gestion_ventes
from .Biens import print_bien, scan_bien
from .Clients import Gestion_clients, print_client
from .Rdv import Gerer_rdv
from .Publicites import Publicite, scan_publicite
from .Personnes import *
from os import system, name
import getpass, re
from time import sleep

class Agence():
    
    """ Initialisation des classes de gestions """
    Clients     = Gestion_clients()
    Ventes      = Gestion_ventes()
    Gestion_rdv = Gerer_rdv()
    Pub         = Publicite()   
    """ Initialisationn de l'utilisateur actuelle """
    client_actuel = None
    client_id     = None
    
    def __init__(self):
        self.listeInfo = {}
    
    """ Function return user data to check login operation """
    def check_user(self, username):
        return self.Clients.login_user(username)
    
    """ Create a new client account """
    def register_client(self):
        type          = input("-- Êtes-vous un Acheteur ou un Vendeur ? ")
        while not type.lower() in ("acheteur", "vendeur"):
            type = input("-- Êtes-vous un Acheteur ou un Vendeur (Entrer Acheteur/Vendeur) ? ")
        print("")
        
        # for validating an Email 
        regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
        # for mobile phonenumber validation
        # 1) Begins with 0 or 33 Then contains 6 or 7 or 8 or 9. Then contains 8 digits 
        Pattern = '(0/33)?[6-9][0-9]{8}'
        
        name          = input("\t -- Nom              : ")
        password      = getpass.getpass("\t -- Mot de passe     : ")
        
        mail          = input("\t -- mail             : ")
        while True:
            if(re.search(regex,mail)):
                break
            mail = input("\t --> entrer mail valide : ")
        
        adress        = input("\t -- adresse          : ")
        
        numTelephone  = input("\t -- Num de telephone : ")
        while True:
            if re.search(Pattern, numTelephone):
                break
            numTelephone  = input("\t --> Entrer num de telephone valide : ")
            
        is_Entreprise = input("-- Êtes-vous une entreprise (O/N) ? ")
        if is_Entreprise.lower() == 'o':
            numSiren       = input("\t -- Num SIREN        : ")
            formeJuridique = input("\t -- Forme Juridique  : ")
            status         = type
        
        if is_Entreprise.lower() == "o":
            self.client_actuel = Entreprise(name, password, numTelephone, adress, mail, numSiren, formeJuridique, status)
        elif type.lower() == "acheteur":
            self.client_actuel = Acheteur(name, password, numTelephone, adress, mail)
        else:
            self.client_actuel = Vendeur(name, password, numTelephone, adress, mail)
        
        if is_Entreprise.lower() == "o":
            self.Clients.add_user(self.client_actuel, True)
        else:
            self.Clients.add_user(self.client_actuel)
        
        self.client_id = self.Clients.login_user(name)[0]
        
        return type

    """ Initilize the actuel client """
    def setClientAcheteur(self, user):
        self.client_id = user[0]
        self.client_actuel = Acheteur(user[2], user[3], user[4], user[5], user[6], user)
    
    def setClientVendeur(self, user):
        self.client_id = user[0]
        self.client_actuel = Vendeur(user[2], user[3], user[4], user[5], user[6])
    
    """ Allows user to see list of properties that match his prefrences"""
    def informerAcheteursPotentiels(self):
        allbiens = self.Ventes.get_items()
        allclients = self.Clients.get_users()
        
        for c in allclients:
            self.listeInfo[c[0]] = [critere for critere in allbiens if (str(critere[2]).lower() == str(c[8]).lower()) and critere[6] != "Vendu"]
        
        return self.listeInfo



    """ ------------------------------- Admin management functions ------------------------------- """
    def show_clients(self):
        space = "-" * 50
        print_centre(space)
        print_centre("Liste des Clients")
        print_centre(space)
        
        users_list = self.Clients.get_users()
        
        for user in users_list:
            print_client(user)
    
    def show_only_seller_clients(self):
        space = "-" * 50
        print_centre(space)
        print_centre("Liste des Clients Vendeur")
        print_centre(space)
        
        users_list = self.Clients.get_users()
        
        for user in users_list:
            if (user[1] == 'vendeur') or (user[1] == 'entreprise' and user[14].lower() == 'vendeur'):
                print_client(user)

    def show_only_buyer_clients(self):
        space = "-" * 50
        print_centre(space)
        print_centre("Liste des Clients Acheteur")
        print_centre(space)
        
        users_list = self.Clients.get_users()
        
        for user in users_list:
            if (user[1] == 'acheteur') or (user[1] == 'entreprise' and user[14] == 'Acheteur'):
                print_client(user)
    
    def print_gestion_menu(self, elem):
        choices = f"""
                1) Ajouter {elem}
                2) Modifier {elem}
                3) Supprimer {elem}
                0) Retourner vers menu principale
              """
        print(choices)
    
    def gerer_client_menu(self):
        while True:
            print_centre("------------------------------- GESTION CLIENTS -------------------------------")
            self.print_gestion_menu("client")
            choice = int(input("\tEntrer votre choix \n ? "))
            if choice==1:
                print('-' * 40)
                new_client = scan_client()
                
                if isinstance(new_client, Entreprise):
                    self.Clients.add_user(new_client, True)
                else:
                    self.Clients.add_user(new_client)
                
                print('-' * 40)
                print(" --> Le client a bien été ajouter")
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                
            elif choice==2:
                id_client = int(input("-- Entrer l'id du client : "))
                
                client_avant = self.Clients.get_user(id_client)
                if len(client_avant) > 0:
                    print("------------------- Données actuel du client -------------------")
                    print_client(client_avant[0])
                    
                    print("------------------- Saisie de nouveau données -------------------")
                    client_nv = scan_client()
                    print('-' * 40)
                    
                    if isinstance(client_nv, Entreprise):
                        self.Clients.update_user(client_nv, id_client, True)
                    else:
                        self.Clients.update_user(client_nv, id_client)
                    
                else:
                    print(f"Aucun client n'est enregistré avec ce id : {id_client}")
                
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                
            elif choice==3:
                id_personne = int(input("-- Entrer l'id du client : "))
                while id_personne < 0:
                    id_personne = int(input("-- Entrer l'id du client (sup à 0): "))
                
                client = self.Clients.get_user(id_personne)
                if len(client) > 0:
                    self.Clients.remove_user(id_personne)
                    print(f'-- > Le client avec id: {id_personne} a été supprimé')
                else:
                    print(f"Aucun client n'est enregistré avec ce id : {id_personne}")
                
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                
            elif choice==0:
                return
            
            else:
                print("\n CHOIX INVALIDE.\n Réessayer !!")
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                clear()
            
            clear()

    def show_liste_rendez_vous(self):
        space = "-" * 50
        print_centre(space)
        print_centre("Liste des rendez-vous")
        print_centre(space)
        
        for rdv in self.Gestion_rdv.get_all():
            print(f'id               : {rdv[0]}')
            print(f'Créneau          : {rdv[1]}')
            if rdv[2]:
                print(f'client vendeur   : {rdv[2]}')
            if rdv[3]:
                print(f'Client acheteur  : {rdv[3]}')
            
            print("-" * 60)
    
    def show_liste_biens(self):
        space = "-" * 50
        print_centre(space)
        print_centre("Liste des Biens")
        print_centre(space)
        
        for bien in self.Ventes.get_items():
            print(f'Proprietaire            : {bien[15]}')
            print_bien(bien)
    
    def show_liste_biens_vendues(self):
        space = "-" * 50
        print_centre(space)
        print_centre("Liste des Biens Vendues")
        print_centre(space)
        
        for bien in self.Ventes.get_items_vendues():
            print(f'Proprietaire            : {bien[15]}')
            print_bien(bien)
    
    def show_liste_biens_en_vente(self):
        space = "-" * 50
        print_centre(space)
        print_centre("Liste des Biens En Ventes")
        print_centre(space)
        
        for bien in self.Ventes.get_items_en_vente():
            print(f'Proprietaire            : {bien[15]}')
            print_bien(bien)
    
    
    def show_liste_pub(self):
        space = "-" * 50
        print_centre(space)
        print_centre("Liste des Publicités")
        print_centre(space)
        
        for pub in self.Pub.get_items():
            print(f'id          : {pub[0]}')
            print(f'Media       : {pub[1]}')
            print(f'Description : {pub[2]}')
            print(f'Bien        : {pub[3]}')
            
            print("-" * 60)
    
    
    def gerer_biens_menu(self):
        while True:
            print_centre("------------------------ GESTION DES BIENS ------------------------")
            self.print_gestion_menu("bien")
            choice = int(input("\tEntrer votre choix \n ? "))
            if choice==1:
                bien = scan_bien()
                print('-' * 40)
                id_vendeur = int(input("-- Entrer l'id du propriétaire du bien : "))
                self.Ventes.add_item(bien, id_vendeur)
                
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                
            elif choice==2:
                id_bien = int(input("-- Entrer l'id du bien : "))
                
                bien_avant = self.Ventes.get_item(id_bien)
                if len(bien_avant) > 0:
                    print("------------------- Données actuel -------------------")
                    print_bien(bien_avant[0])
                    
                    print("------------------- Saisie de nouveau données -------------------")
                    bien = scan_bien()
                    print('-' * 40)
                    
                    id_vendeur = int(input("-- Entrer l'id du propriétaire du bien : "))
                    
                    self.Ventes.update_item(bien, id_bien, id_vendeur)
                else:
                    print(f"Aucun bien n'est enregistré avec ce id : {id_bien}")
                
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                
            elif choice==3:
                id_bien = int(input("-- Entrer l'id du bien : "))
                while id_bien < 0:
                    id_bien = int(input("-- Entrer l'id du bien (sup à 0): "))
                
                bien_avant = self.Ventes.get_item(id_bien)
                if len(bien_avant) > 0:
                    self.Ventes.remove_item(id_bien)
                    print(f'-- > Le bien avec comme id: {id_bien} a été supprimé.')
                else:
                    print(f"Aucun bien n'est enregistré avec ce id : {id_bien}")
                
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                
            elif choice==0:
                return
            
            else:
                print("\n CHOIX INVALIDE.\n Réessayer !!")
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                clear()
            
            clear()
    
    
    def gerer_publicites_menu(self):
        while True:
            print_centre("------------------------ GESTION DES PUBLICITES ------------------------")
            self.print_gestion_menu("publicité")
            
            choice = int(input("\tEntrer votre choix \n ? "))
            clear()
            
            if choice==1:
                print_centre("------------------------ Creer Pub ------------------------")
                media = scan_publicite()
                id_bien = int(input("Entrer id du bien : "))
                self.Pub.add_item(media, id_bien)
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                
            elif choice==2:
                print_centre("------------------------ Modifier Pub ------------------------")
                id_pub = int(input("Entrer id du publicité : "))
                
                pub_avant = self.Pub.get_item(id_pub)
                if len(pub_avant) > 0:
                    print("------------------- Données actuel -------------------")
                    print(f'id          : {pub_avant[0][0]}')
                    print(f'Media       : {pub_avant[0][1]}')
                    print(f'Description : {pub_avant[0][2]}')
                    print(f'Bien        : {pub_avant[0][3]}')
            
                    print("------------------- Saisie de nouveau données -------------------")
                    media = scan_publicite()
                    id_bien = int(input("Entrer id du bien : "))
                    
                    self.Pub.update_item(media, id_pub, id_bien)
                else:
                    print(f"Aucune publicité n'est enregistré avec ce id : {id_pub}")
                
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                
            elif choice==3:
                print_centre("------------------------ Supprimer Pub ------------------------")
                id_pub = int(input("-- Entrer l'id du publicité : "))
                while id_pub < 0:
                    id_pub = int(input("-- Entrer l'id du publicité (sup à 0): "))
                
                pub_avant = self.Pub.get_item(id_pub)
                if len(pub_avant) > 0:
                    self.Pub.remove_item(id_pub)
                    print(f'-- > La publicité avec id: {id_pub} a été supprimé')
                else:
                    print(f"Aucune publicité n'est enregistré avec ce id : {id_pub}")
                
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                
            elif choice==0:
                return
            
            else:
                print("\n CHOIX INVALIDE.\n Réessayer !!")
                input(" -- > Cliquez sur Entrée pour revenir au menu ")
                clear()
            
            clear()

def clear(): 
    # for windows 
    if name == 'nt': 
        _ = system('cls') 
    # for mac and linux
    else: 
        _ = system('clear') 