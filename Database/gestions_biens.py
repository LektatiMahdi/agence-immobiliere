import sqlite3
from Classes.Biens import Terrain, Maison, Appt

conn = sqlite3.connect('sqlite.db')
# cursor
c = conn.cursor()

c.execute("""
            CREATE TABLE IF NOT EXISTS Biens (
                bien_id INTEGER PRIMARY KEY AUTOINCREMENT,
                type TEXT,
                adresse TEXT,
                orientation TEXT,
                dateVente TEXT,
                dateDispo TEXT,
                status TEXT,
                prix REAL,
                surfaceSol REAL,
                longeurFacade REAL,
                surfaceHabitable REAL,
                nbPieces INTEGER,
                nbEtages INTEGER,
                moyenChauffage TEXT,
                chargesMensuelles REAL,
                "id_vendeur" INTEGER REFERENCES Personnes(personne_id)
            )
          """)

def insert_bien(bien, id_vendeur):
    if isinstance(bien, Terrain):
        with conn:
            c.execute("""
                      INSERT INTO Biens
                        ( type,
                        adresse,
                        orientation,
                        dateVente,
                        dateDispo,
                        status,
                        prix,
                        surfaceSol,
                        longeurFacade,
                        id_vendeur)
                      VALUES (:type, :adresse, :orientation, :dateVente, :dateDispo, :status, :prix, :surfaceSol,   :longeurFacade, :id_vendeur)
                      """,
                        {
                            'type': 'terrain',
                            'adresse': bien.adresse,
                            'orientation': bien.orientation, 
                            'dateVente': bien.dateVente, 
                            'dateDispo': bien.dateDispo, 
                            'status': bien.status, 
                            'prix': bien.prix,
                            'surfaceSol': bien.surfaceSol,
                            'longeurFacade': bien.longeurFacade,
                            'id_vendeur': id_vendeur
                        })
    if isinstance(bien, Maison):
        with conn:
            c.execute("""
                      INSERT INTO Biens
                        ( type,
                        adresse,
                        orientation,
                        dateVente,
                        dateDispo,
                        status,
                        prix,
                        surfaceHabitable,
                        nbPieces,
                        nbEtages,
                        moyenChauffage,
                        id_vendeur
                        )
                      VALUES (:type, :adresse, :orientation, :dateVente, :dateDispo, :status, :prix, :surfaceHabitable,   :nbPieces, :nbEtages, :moyenChauffage, :id_vendeur)
                      """,
                        {
                            'type': 'maison',
                            'adresse': bien.adresse,
                            'orientation': bien.orientation, 
                            'dateVente': bien.dateVente, 
                            'dateDispo': bien.dateDispo, 
                            'status': bien.status, 
                            'prix': bien.prix,
                            'surfaceHabitable': bien.surfaceHabitable,
                            'nbPieces': bien.nbPieces,
                            'nbEtages': bien.nbEtages,
                            'moyenChauffage': bien.moyenChauffage,
                            'id_vendeur': id_vendeur
                        })
    if isinstance(bien, Appt):
        with conn:
            c.execute("""
                      INSERT INTO Biens
                        ( type,
                        adresse,
                        orientation,
                        dateVente,
                        dateDispo,
                        status,
                        prix,
                        nbPieces,
                        nbEtages,
                        chargesMensuelles,
                        id_vendeur
                        )
                      VALUES (:type, :adresse, :orientation, :dateVente, :dateDispo, :status, :prix, :nbPieces, :nbEtages, :chargesMensuelles, :id_vendeur)
                      """,
                        {
                            'type': 'appart',
                            'adresse': bien.adresse,
                            'orientation': bien.orientation, 
                            'dateVente': bien.dateVente, 
                            'dateDispo': bien.dateDispo, 
                            'status': bien.status, 
                            'prix': bien.prix,
                            'nbPieces': bien.nbPieces,
                            'nbEtages': bien.nbEtages,
                            'chargesMensuelles': bien.chargesMensuelles,
                            'id_vendeur': id_vendeur
                        })

def get_biens_by_type(type_bien):
    c.execute("SELECT * FROM Biens WHERE type=:type", {'type': type_bien})
    return c.fetchall()

def get_idVendeur_by_idBien(id_bien):
    c.execute("SELECT id_vendeur FROM Biens WHERE bien_id=:id", {'id': id_bien})
    return c.fetchall()

def get_bien_by_vendeur(id_vendeur):
    c.execute("SELECT * FROM Biens WHERE id_vendeur=?", (id_vendeur,))
    return c.fetchall()

def get_biens():
    c.execute("SELECT * FROM Biens")
    return c.fetchall()

def get_bien(id):
    c.execute("SELECT * FROM Biens WHERE bien_id=?", (id,))
    return c.fetchall()

def get_biens_en_vente():
    statut = "En vente"
    c.execute("SELECT * FROM Biens WHERE status=?", (statut,))
    return c.fetchall()

def get_biens_vendues():
    statut = "Vendu"
    c.execute("SELECT * FROM Biens WHERE status=?", (statut,))
    return c.fetchall()

def update_bien(bien, id, id_vendeur):
    sqlite_update_query = """
                      UPDATE Biens SET 
                        type=:type,
                        adresse=:adresse, 
                        orientation=:orientation,
                        dateVente=:dateVente, 
                        dateDispo=:dateDispo, 
                        status=:status, 
                        prix=:prix, 
                        surfaceSol=:surfaceSol, 
                        longeurFacade=:longeurFacade,
                        surfaceHabitable=:surfaceHabitable,
                        nbPieces=:nbPieces,
                        nbEtages=:nbEtages,
                        chargesMensuelles=:chargesMensuelles,
                        moyenChauffage=:moyenChauffage,
                        id_vendeur=:id_vendeur
                      WHERE
                        bien_id=:bien_id
                    """
    if isinstance(bien, Terrain):
        new_data = {
                    'bien_id': id,
                    'type': 'terrain',
                    'adresse': bien.adresse,
                    'orientation': bien.orientation, 
                    'dateVente': bien.dateVente, 
                    'dateDispo': bien.dateDispo, 
                    'status': bien.status, 
                    'prix': bien.prix,
                    'surfaceSol': bien.surfaceSol,
                    'longeurFacade': bien.longeurFacade,
                    'surfaceHabitable': None,
                    'nbPieces': None,
                    'nbEtages': None,
                    'chargesMensuelles': None,
                    'moyenChauffage': None,
                    'id_vendeur':id_vendeur
                  }
        with conn:
            c.execute(sqlite_update_query, new_data)
            
    if isinstance(bien, Maison):
        new_data = {
                      'bien_id': id,
                      'type': 'maison',
                      'adresse': bien.adresse,
                      'orientation': bien.orientation, 
                      'dateVente': bien.dateVente, 
                      'dateDispo': bien.dateDispo, 
                      'status': bien.status, 
                      'prix': bien.prix,
                      'surfaceSol': None,
                      'longeurFacade': None,
                      'surfaceHabitable': bien.surfaceHabitable,
                      'nbPieces': bien.nbPieces,
                      'nbEtages': bien.nbEtages,
                      'chargesMensuelles': None,
                      'moyenChauffage': bien.moyenChauffage,
                      'id_vendeur':id_vendeur
                  }
        with conn:
            c.execute(sqlite_update_query, new_data)
            
    if isinstance(bien, Appt):
        new_data = {
                      'bien_id': id,
                      'type': 'appart',
                      'adresse': bien.adresse,
                      'orientation': bien.orientation, 
                      'dateVente': bien.dateVente, 
                      'dateDispo': bien.dateDispo, 
                      'status': bien.status, 
                      'prix': bien.prix,
                      'surfaceSol': None,
                      'longeurFacade': None,
                      'surfaceHabitable': None,
                      'nbPieces': bien.nbPieces,
                      'nbEtages': bien.nbEtages,
                      'chargesMensuelles': bien.chargesMensuelles,
                      'moyenChauffage': None,
                      'id_vendeur':id_vendeur
                  }                    
        with conn:
            c.execute(sqlite_update_query, new_data)

def update_bien_to_sold(id_vendeur):
    sqlite_update_query = "UPDATE Biens SET status=:status WHERE id_vendeur=:id_vendeur "
    new_data = {
                    'id_vendeur': id_vendeur,
                    'status': 'Vendu'
                }
    with conn:
        c.execute(sqlite_update_query, new_data)

def remove_bien(id):
    with conn:
        c.execute("DELETE from Biens WHERE bien_id=?", (id,))

conn.commit()
# conn.close()