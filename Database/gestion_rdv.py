import sqlite3

conn = sqlite3.connect('sqlite.db')
# cursor
c = conn.cursor()

c.execute("""
            CREATE TABLE IF NOT EXISTS Rdv (
                rdv_id INTEGER PRIMARY KEY AUTOINCREMENT,
                date TEXT,
                "client1_id" INTEGER REFERENCES Personnes(personne_id),
                "client2_id" INTEGER REFERENCES Personnes(personne_id)
            )
          """)

def insert_rdv(rdv):
    #is_exist = check_if_exists(rdv.date, rdv.id1)
    #if len(is_exist) == 0:
    with conn:
        try:
            sql_query = " INSERT INTO Rdv (date, client1_id, client2_id) VALUES (?, ?, ?) "
            data = (rdv.date, rdv.id1, rdv.id2)
            c.execute(sql_query, data)
        except sqlite3.Error as error:
            print("Could not insert data")
    #else:
    #print("You already have an appointment with this date".center(100))

def check_if_exists(date, client1_id=None, client2_id=None):
    c.execute("SELECT * FROM Rdv WHERE date=? AND client1_id=?", (date, client1_id))
    return c.fetchall()

def update_rdv(id, new_rdv):
    with conn:
        try:
            sql_update_query = """
                                    UPDATE Rdv SET 
                                        date=:date,
                                        client1_id=:client1_id,
                                        client2_id=:client2_id
                                    WHERE 
                                        rdv_id=:rdv_id
                                """
            new_data = {
                'rdv_id': id,
                'date': new_rdv.date,
                'client1_id': new_rdv.id1,
                'client2_id': new_rdv.id2
            }
            conn.execute(sql_update_query, new_data)
        except sqlite3.Error as error:
            print("Could not update data")

def get_rdv_all():
    c.execute("SELECT * FROM Rdv")
    return c.fetchall()

def get_rdv(id):
    c.execute("SELECT * FROM Rdv WHERE rdv_id=?", (id,))
    return c.fetchall()

def get_rdv_by_date(date):
    c.execute("SELECT * FROM Rdv WHERE date=?", (date,))
    return c.fetchall()

def get_rdv_by_client_vendeur(client1_id):
    c.execute("SELECT * FROM Rdv WHERE client1_id=?", (client1_id,))
    return c.fetchall()

def get_rdv_by_client_acheteur(client2_id):
    c.execute("SELECT * FROM Rdv WHERE client2_id=?", (client2_id,))
    return c.fetchall()

def remove_rdv(id):
    try:
        with conn:
            c.execute("DELETE from Rdv WHERE rdv_id=?", (id,))
    except sqlite3.Error as error:
        print("Could not remove data")