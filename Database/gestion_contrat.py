import sqlite3

conn = sqlite3.connect('sqlite.db')
# cursor
c = conn.cursor()

""" Query to create mandat table (between seller and agency) """
c.execute("""
            CREATE TABLE IF NOT EXISTS Mandat (
                mandat_id INTEGER PRIMARY KEY AUTOINCREMENT,
                duree TEXT,
                "vendeur_id" INTEGER REFERENCES Personnes(personne_id)
            )
          """)

c.execute("""
            CREATE TABLE IF NOT EXISTS PromesseVente (
                Vente_id INTEGER PRIMARY KEY AUTOINCREMENT,
                adresseNotaire TEXT,
                prixVerse REAL,
                dateDeVente TEXT,
                comissionAgence REAL,
                fraisDeVente REAL,
                est_pourcentage_versee INTEGER,
                "bien_id" INTEGER REFERENCES Biens(bien_id),
                "acheteur_id" INTEGER REFERENCES Personnes(personne_id),
                "vendeur_id" INTEGER REFERENCES Personnes(personne_id)
            )
          """)

""" ---------------------------------- CRUD Operations on mandat table ---------------------------------- """
def insert_mandat(mandat):
    with conn:
        try:
            sql_query = " INSERT INTO Mandat (duree, vendeur_id) VALUES (?, ?) "
            data = (mandat.duree, mandat.id_vendeur)
            c.execute(sql_query, data)
        except sqlite3.Error as error:
            print("Could not insert data")

def update_mandat(id, new_mandat):
    with conn:
        try:
            sql_update_query = """
                                    UPDATE Mandat SET 
                                        duree=:duree,
                                        vendeur_id=:vendeur_id
                                    WHERE 
                                        mandat_id=:mandat_id
                                """
            new_data = {
                'mandat_id': id,
                'duree': new_mandat.duree,
                'vendeur_id': new_mandat.id_vendeur
            }
            conn.execute(sql_update_query, new_data)
        except sqlite3.Error as error:
            print("Could not update data")

def get_all_mandat():
    c.execute("SELECT * FROM Mandat")
    return c.fetchall()

def get_mandat(id):
    c.execute("SELECT * FROM Mandat WHERE mandat_id=?", (id,))
    return c.fetchall()

def remove_mandat(id):
    try:
        with conn:
            c.execute("DELETE from Mandat WHERE mandat_id=?", (id,))
    except sqlite3.Error as error:
        print("Could not remove data")




""" ----------------------------- CRUD Operations on Sale promesses table ------------------------------- """

def insert_promesse(vente_promesse):
    with conn:
        try:
            sql_query = """ INSERT INTO PromesseVente (
                                adresseNotaire,
                                prixVerse,
                                dateDeVente,
                                comissionAgence,
                                fraisDeVente,
                                est_pourcentage_versee,
                                bien_id,
                                acheteur_id,
                                vendeur_id
                                )
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
                        """
            data = (vente_promesse.adresseNotaire, 
                    vente_promesse.prixVerse,
                    vente_promesse.dateDeVente,
                    vente_promesse.comissionAgence,
                    vente_promesse.fraisDeVente,
                    vente_promesse.est_pourcentage_versee,
                    vente_promesse.bien_id,
                    vente_promesse.acheteur_id,
                    vente_promesse.vendeur_id
                    )
            c.execute(sql_query, data)
        except sqlite3.Error as error:
            # print("Could not insert data")
            print(error)

def update_promesse(vente_id, new_vente_promesse):
    with conn:
        try:
            sql_update_query = """
                                    UPDATE PromesseVente SET 
                                        adresseNotaire=:adresseNotaire,
                                        prixVerse=:prixVerse,
                                        dateDeVente=:dateDeVente,
                                        comissionAgence=:comissionAgence,
                                        fraisDeVente=:fraisDeVente,
                                        est_pourcentage_versee=:est_pourcentage_versee,
                                        bien_id=:bien_id,
                                        acheteur_id=:acheteur_id,
                                        vendeur_id=:vendeur_id
                                    WHERE 
                                        Vente_id=:Vente_id
                                """
            new_data = {
                'Vente_id': vente_id,
                'adresseNotaire': new_vente_promesse.adresseNotaire,
                'prixVerse': new_vente_promesse.prixVerse,
                'dateDeVente': new_vente_promesse.dateDeVente,
                'comissionAgence': new_vente_promesse.comissionAgence,
                'fraisDeVente': new_vente_promesse.fraisDeVente,
                'est_pourcentage_versee': new_vente_promesse.est_pourcentage_versee,
                'bien_id': new_vente_promesse.bien_id,
                'acheteur_id': new_vente_promesse.acheteur_id,
                'vendeur_id': new_vente_promesse.vendeur_id
            }
            conn.execute(sql_update_query, new_data)
        except sqlite3.Error as error:
            print("Could not update data")

def get_all_promesses():
    c.execute("SELECT * FROM PromesseVente")
    return c.fetchall()

def get_promesse(id):
    c.execute("SELECT * FROM PromesseVente WHERE Vente_id=?", (id,))
    return c.fetchall()


def select_promesse_by_acheteur(id_acheteur):
    c.execute("SELECT * FROM PromesseVente WHERE acheteur_id=?", (id_acheteur,))
    return c.fetchall()

def select_promesse_by_vendeur(id_vendeur):
    c.execute("SELECT * FROM PromesseVente WHERE vendeur_id=?", (id_vendeur,))
    return c.fetchall()

def remove_promesse(id_acheteur):
    try:
        with conn:
            c.execute("DELETE from PromesseVente WHERE acheteur_id=?", (id_acheteur,))
    except sqlite3.Error as error:
        print("Could not remove data")