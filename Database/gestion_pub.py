import sqlite3, os
from Database.gestions_biens import *

sqliteConnection = sqlite3.connect('sqlite.db')
# cursor
cursor = sqliteConnection.cursor()

cursor.execute("""
            CREATE TABLE IF NOT EXISTS Publicites (
                pub_id INTEGER PRIMARY KEY AUTOINCREMENT,
                media TEXT NOT NULL,
                photo BLOB,
                video BLOB,
                description TEXT,
                "bien_id" INTEGER REFERENCES Biens(bien_id)
            )
          """)

def convertToBinaryData(filename):
    #Convert digital data to binary format
    try:
        with open(filename, 'rb') as file:
            blobData = file.read()
        return blobData
    except OSError as e:
        return 0

def writeTofile(data, filename):
    try:
        # Convert binary data to proper format and write it on Hard Disk
        with open(filename, 'wb') as file:
            file.write(data)
        print("Stored blob data into: ", filename, "\n")
    except OSError as e:
        return 0

def insert_pub(pub, id_bien):
    try:
        sqlite_insert_pub_query = """ INSERT INTO Publicites
                                  (media, photo, video, description, bien_id) VALUES (?, ?, ?, ?, ?)"""
        
        bien = get_bien(id_bien)
        if len(bien) != 0:
            pubPhoto = convertToBinaryData(pub.photo)
            pubVideo = convertToBinaryData(pub.video)
            # Convert data into tuple format
            data_tuple = (pub.media, pubPhoto, pubVideo, pub.description, id_bien)
            cursor.execute(sqlite_insert_pub_query, data_tuple)
            sqliteConnection.commit()
            print("Image and video inserted successfully as a BLOB into a table")
            cursor.close()
        else:
            print("Please provide a valid id")
            return 0

    except sqlite3.Error as error:
        print("Failed to insert blob data into sqlite table", error)

def get_publicites():
    try:
        cursor = sqliteConnection.cursor()
        cursor.execute("SELECT * from Publicites")
        record = cursor.fetchall()
        data = []
        for row in record:
            # print("Id = ", row[0], "Media = ", row[1], "Description = ", row[4])
            id=row[0]
            media=row[1]
            description  = row[4]
            photo = row[2]
            video = row[3]
            id_bien = row[5]

            photoPath = f'{os.getcwd()}\db_data\{id}.jpg'
            videoPath = f'{os.getcwd()}\db_data\{id}_video.mp4'
            writeTofile(photo, photoPath)
            writeTofile(video, videoPath)
            data.append((id, media, description, id_bien))
            
        return data
    except sqlite3.Error as error:
        print("Failed to read blob data from sqlite table", error)

def get_publicite(pubId):
    # cursor = sqliteConnection.cursor()
    cursor.execute("SELECT * from Publicites where pub_id = ?", (pubId,))
    return cursor.fetchall()

def update_pub(pub, id, id_bien):
    with sqliteConnection:
        cursor.execute("""
                      UPDATE Publicites SET 
                        media=:media, photo=:photo, video=:video, description=:description, bien_id=:bien_id
                        WHERE
                        pub_id=:pub_id
                      """,
                        {
                            'pub_id': id,
                            'media': pub.media,
                            'description': pub.description,
                            'photo': pub.photo, 
                            'video': pub.video,
                            'bien_id':id_bien
                        })
    
def remove_pub(id):
    with sqliteConnection:
        cursor.execute("DELETE from Publicites WHERE pub_id=?", (id,))

sqliteConnection.commit()