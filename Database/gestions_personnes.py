import shutil
import sqlite3, sys
from Classes.Personnes import Acheteur, Vendeur, Entreprise
from .login_security import *

conn = sqlite3.connect('sqlite.db')
# cursor
c = conn.cursor()

c.execute("""
            CREATE TABLE IF NOT EXISTS Personnes (
                personne_id INTEGER PRIMARY KEY AUTOINCREMENT,
                type TEXT,
                nom TEXT,
                password TEXT,
                numTel INTEGER,
                adresse TEXT,
                mail TEXT,
                prixSouhaite REAL,
                localisation TEXT,
                surfaceSol INTEGER,
                nbPieces INTEGER,
                typeBienVoulu TEXT,
                numSIREN INTEGER,
                formeJuridique TEXT,
                statut TEXT
            )
          """)
          
def insert_personne(personne, is_entreprise=False):
    password = password_encrypt(personne.password.encode(), 'mypass')
    if is_entreprise:
        with conn:
            c.execute("""
                      INSERT INTO Personnes
                        ( type,
                        nom,
                        password,
                        numTel,
                        adresse,
                        mail,
                        numSIREN,
                        formeJuridique,
                        statut,
                        prixSouhaite,
                        localisation,
                        surfaceSol,
                        nbPieces,
                        typeBienVoulu
                        )
                      VALUES (:type, :nom, :password, :numTel, :adresse, :mail, :numSIREN, :formeJuridique, :statut, :prixSouhaite, :localisation, :surfaceSol, :nbPieces, :typeBienVoulu )
                      """,
                        {
                            'type': 'entreprise',
                            'nom': personne.nom,
                            'password': password,
                            'numTel': personne.numTel,
                            'adresse': personne.adresse, 
                            'mail': personne.mail,
                            'numSIREN': personne.numSIREN,
                            'formeJuridique': personne.formeJuridique,
                            'statut': personne.statut,
                            'prixSouhaite': personne.prixSouhaite, 
                            'localisation': personne.localisation,
                            'surfaceSol': personne.surfaceSol,
                            'nbPieces': personne.nbPieces,
                            'typeBienVoulu': personne.typeBienVoulu
                        })
        return
    else:
        if isinstance(personne, Acheteur):
            with conn:
                c.execute("""
                          INSERT INTO Personnes
                            ( type,
                            nom,
                            password,
                            numTel,
                            adresse,
                            mail,
                            prixSouhaite,
                            localisation,
                            surfaceSol,
                            nbPieces,
                            typeBienVoulu)
                          VALUES (:type, :nom, :password, :numTel, :adresse, :mail, :prixSouhaite, :localisation, :surfaceSol, :nbPieces, :typeBienVoulu)
                          """,
                            {
                                'type': 'acheteur',
                                'nom': personne.nom,
                                'password': password,
                                'numTel': personne.numTel,
                                'adresse': personne.adresse, 
                                'mail': personne.mail, 
                                'prixSouhaite': personne.prixSouhaite, 
                                'localisation': personne.localisation, 
                                'surfaceSol': personne.surfaceSol,
                                'nbPieces': personne.nbPieces,
                                'typeBienVoulu': personne.typeBienVoulu
                            })
        if isinstance(personne, Vendeur):
            with conn:
                c.execute("""
                          INSERT INTO Personnes
                            ( type,
                            nom,
                            password,
                            numTel,
                            adresse,
                            mail
                            )
                          VALUES (:type, :nom, :password, :numTel, :adresse, :mail)
                          """,
                            {
                                'type': 'vendeur',
                                'nom': personne.nom,
                                'password': password,
                                'numTel': personne.numTel,
                                'adresse': personne.adresse, 
                                'mail': personne.mail
                            })

def get_personnes():
    c.execute("SELECT * FROM Personnes")
    return c.fetchall()

def get_personne(personne_id):
    c.execute("SELECT * FROM Personnes WHERE personne_id=:personne_id", {'personne_id': personne_id})
    return c.fetchall()

def get_personne_by_type(type_personne):
    c.execute("SELECT * FROM Personnes WHERE type=:type", {'type': type_personne})
    return c.fetchall()

def update_personne(personne, id, is_entreprise=False):
    password = password_encrypt(personne.password.encode(), 'mypass')
    if is_entreprise:
        sqlite_update_entreprise_query = """
                                      UPDATE Personnes SET
                                        type=:type,
                                        nom=:nom,
                                        password=:password,
                                        numTel=:numTel,
                                        adresse=:adresse,
                                        mail=:mail,
                                        numSIREN=:numSIREN,
                                        formeJuridique=:formeJuridique,
                                        statut=:statut,
                                        prixSouhaite=:prixSouhaite,
                                        localisation=:localisation,
                                        surfaceSol=:surfaceSol,
                                        nbPieces=:nbPieces,
                                        typeBienVoulu=:typeBienVoulu
                                      WHERE
                                        personne_id=:id
                                    """
        new_data = {
                            'id': id,
                            'type': 'entreprise',
                            'nom': personne.nom,
                            'password': password,
                            'numTel': personne.numTel,
                            'adresse': personne.adresse, 
                            'mail': personne.mail,
                            'numSIREN': personne.numSIREN,
                            'formeJuridique': personne.formeJuridique,
                            'statut': personne.statut,
                            'prixSouhaite': personne.prixSouhaite, 
                            'localisation': personne.localisation,
                            'surfaceSol': personne.surfaceSol,
                            'nbPieces': personne.nbPieces,
                            'typeBienVoulu': personne.typeBienVoulu
                        }
        with conn:
            c.execute(sqlite_update_entreprise_query, new_data)
        return
    else:
        if isinstance(personne, Acheteur):
            sqlite_update_acheteur_query = """
                                          UPDATE Personnes SET 
                                            type=:type,
                                            nom=:nom,
                                            password=:password,
                                            numTel=:numTel,
                                            adresse=:adresse,
                                            mail=:mail,
                                            prixSouhaite=:prixSouhaite,
                                            localisation=:localisation,
                                            surfaceSol=:surfaceSol,
                                            nbPieces=:nbPieces,
                                            typeBienVoulu=:typeBienVoulu
                                          WHERE
                                            personne_id=:id
                                        """
            new_data = {
                                'id': id,
                                'type': 'acheteur',
                                'nom': personne.nom,
                                'password': password,
                                'numTel': personne.numTel,
                                'adresse': personne.adresse, 
                                'mail': personne.mail, 
                                'prixSouhaite': personne.prixSouhaite, 
                                'localisation': personne.localisation, 
                                'surfaceSol': personne.surfaceSol,
                                'nbPieces': personne.nbPieces,
                                'typeBienVoulu': personne.typeBienVoulu
                            }
            with conn:
                c.execute(sqlite_update_acheteur_query, new_data)
                
        if isinstance(personne, Vendeur):
            sqlite_update_vendeur_query = """
                                          UPDATE Personnes SET 
                                            type=:type,
                                            nom=:nom,
                                            password=:password,
                                            numTel=:numTel,
                                            adresse=:adresse,
                                            mail=:mail
                                          WHERE
                                            personne_id=:id
                                        """
            new_data = {
                                'id': id,
                                'type': 'vendeur',
                                'nom': personne.nom,
                                'password': password,
                                'numTel': personne.numTel,
                                'adresse': personne.adresse, 
                                'mail': personne.mail 
                    
                            }
            with conn:
                c.execute(sqlite_update_vendeur_query, new_data)


def remove_personne(id):
    with conn:
        c.execute("DELETE from Personnes WHERE personne_id=?", (id,))

def get_user(username):
    c.execute("SELECT * FROM Personnes WHERE nom=:nom", {'nom': username})
    try:
        data = c.fetchall()[0]
        result = list(data)
        result[3] = password_decrypt(result[3], 'mypass').decode() # decrypt the token
        return result
    except IndexError:
        print_centre("User does not exist")
        return []

def print_centre(s):
    print(s.center(shutil.get_terminal_size().columns))
