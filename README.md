# Agence Immobilière

***
L’agence immobilière Timmo se charge de vendre des biens immobiliers (appartements, maisons et terrains). 
Ce programme Timmo est un système informatique pour la gestion de cette agence.

## Table de matières
1. [Info génerale](#info-génerale)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Choix implementation et notion POO](#implementation-poo)
5. [Améliorations](#améliorations)

## Info génerale
***
Ce programme permet la gestion de l'agence immobiliére ainsi il est aussi destinée au client. 
* Apres lancement, le programme demande à l'utilisateur d'effectuer son authentification pour decider si le client est un administrateur ou bien un client ou bien s'il veut creer son compte.

* Pour acceder au compte Admin : <br />
**Nom d'utilisateur**   : Admin  <br />
**Mot de passe**        : Admin
* Pour le client, sur premiere connection au programme il faudra créer un compte (suiver les instructions sur le prog)

* Si l'utilisateur est un Admin, le programme lui propose un menu avec multiple options:
1. **Gerer les clients** (Consulter la liste avec filtrage / Ajouter - Modifier - Supprimer)
2. **Gerer les Rdv** (Consulter la liste des rdv)
3. **Gerer les biens** (Consulter la liste avec filtrage('Vendu' - 'En Vente') / Ajouter - Modifier - Supprimer)
4. **Gerer les publicités** (Consulter la liste / Ajouter - Modifier - Supprimer)

* Si l'utilisateur est un client Acheteur, le programme lui propose un menu avec multiple options:
1. **Indiquer ses preferences** 
2. **Consulter ses Rdv** 
3. **Consulter les biens selon preferences indiquees** 
4. **Consulter tous les biens** 
5. **Achat** (realiser un achat - Signer promesse de vente (necessite la prise d'un rdv avec l'acheteur) - annuler son achat)

* Si l'utilisateur est un client Vendeur, le programme lui propose un menu avec multiple options:
1. **Contacter l'agence**  (Fixer un rdv - definir les details de son bien à vendre - signer mandat de vente)
2. **Consulter ses Rdv** 
3. **Consulter ses biens mis en ventes** 
4. **Signer la promesse de vente**

* <mark>Notez que l'utilisateur client doit avoir un rendez-vous et que ce doit être l'heure exacte du rendez-vous lorsque vous choisissez ces options (Inserer donnees du bien - Signer mandat de vente - Signer promesse de vente)</mark>

## Technologies
***
La liste des technologies utilisées dans ce programme:
* [Python](https://www.python.org/): Version 3.9
* [SQLite module](https://docs.python.org/3/library/sqlite3.html): Version 3
* [Pytest module](https://docs.pytest.org/en/stable/getting-started.html): Version 6.2.2

## Installation

### Installer Python:
***
Rendez-vous sur le site de Python. En vous rendant à l'adresse: https://www.python.org/ vous allez pouvoir télécharger votre version de Python. Le site détectera que vous êtes sous Windows et la version que vous utilisez : il affichera alors l'installateur Windows - Mac ou Linux approprié.

### Installer Pytest:
***
pytest est un framework qui facilite la création de tests simples et évolutifs.
Pour installer Pytest, executer la commande suivante dans le terminal : **pip install -U pytest**

### Pour lancer le programme:
***
- Lorsque Python est installé sous Windows, La console de commande est un programme comme un autre qui se nomme cmd.exe. Il faut donc cliquer sur le bouton Windows et en tapant les lettres **cmd**, Windows nous propose d’ouvrir la console de commande. Maintenant, il faut naviguer vers le dossier où se trouve notre script. La commande **cd** (Change Directory) permet de changer le répertoire dans lequel on se trouve. Mais où se trouve-t-on en fait ? Analysons un instant l’invite fournie par Windows. Dans la plupart des cas, elle affiche : "C:\Users\your_user_name> alors ce qu'il faut faire maintenant c'est de taper la commande : **cd "\path_to_file"**
- Une fois dans le dossier où se trouve notre script, il ne nous reste plus qu’à l’exécuter en tapant : **python Timmo.py**

## Choix implementation et notion POO:
***
* Le devloppement de ce projet est basée sur la notion d'heritage qui etait assez interessante pour modeliser les differents client avec leud difference (Acheteur - Vendeur - Entreprise)
* Ainsi pour avoir une solide base on a pris la décision d'utiliser le module sqlite qui nous permet de provider plus de fonctionnaliter à l'administrateur ainsi que les cliens (toutes les fonctions d'ajout - modification - suppression ...) Ce qui fait qu'on devait modéliser une base de données pour la structure de notre projet

## Améliorations
***
* Apres la premiere modelisation de la base de données, on trouve que ça sera plus clair de separer le client acheteur et le client vendeur puisqu'ils ont deux comportements differents, ce qui donne cette modelisation :
![](Database/DiagrammeEA.png)

* Il faut ajouter des fonctions de nettoyage de la base de données apres le délai precisé
* Enfin, il faut avoir une base de données sur un serveur pour que le programme soit accessible partout